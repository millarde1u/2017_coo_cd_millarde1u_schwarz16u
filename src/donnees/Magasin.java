package donnees;

import java.util.ArrayList;

/**
 * La classe Magasin represente un magasin qui vend des CDs.</p>
 * 
 * cette classe est caracterisee par un ensemble de CDs correspondant aux CDS
 * vendus dans ce magasin.
 * 
 */
public class Magasin {

	/**
	 * la liste des CDs disponibles en magasin
	 */
	private ArrayList<CD> listeCds;

	/**
	 * construit un magasin par defaut qui ne contient pas de CD
	 */
	public Magasin() {
		listeCds = new ArrayList<CD>();
	}

	/**
	 * ajoute un cd au magasin
	 * 
	 * @param cdAAjouter
	 *            le cd a ajouter
	 */
	public void ajouteCd(CD cdAAjouter) {
		listeCds.add(cdAAjouter);
	}

	@Override
	/**
	 * affiche le contenu du magasin
	 */
	public String toString() {
		String chaineResultat = "";
		//parcours des Cds
		for (int i = 0; i < listeCds.size(); i++) {
			chaineResultat += listeCds.get(i);
		}
		chaineResultat += "nb Cds: " + listeCds.size();
		return (chaineResultat);

	}

	/**
	 * @return le nombre de Cds du magasin
	 */
	public int getNombreCds() {
		return listeCds.size();
	}
	
	/**
	 * permet d'acceder � un CD
	 * 
	 * @return le cd a l'indice i ou null si indice est non valide
	 */
	public CD getCd(int i)
	{
		CD res=null;
		if ((i>=0)&&(i<this.listeCds.size()))
			res=this.listeCds.get(i);
		return(res);
	}

	public void trierAlbum() {
		ArrayList<CD> liste_temp=new ArrayList<CD>();
		while(listeCds.size() != 0){
			CD cd_tmp =listeCds.get(0);
			for(CD n:listeCds){
				if (n.compareNom(cd_tmp)<0){
					cd_tmp = n;
				}
			}
			liste_temp.add(cd_tmp);
			listeCds.remove(cd_tmp);
		}
		listeCds = liste_temp;
	}
	
	public void trierAriste(){
		ArrayList<CD> liste_temp=new ArrayList<CD>();
		while(listeCds.size() != 0){
			CD cd_tmp =listeCds.get(0);
			for(CD n:listeCds){
				if (n.compareArtiste(cd_tmp)<0){
					cd_tmp = n;
				}
			}
			liste_temp.add(cd_tmp);
			listeCds.remove(cd_tmp);
		}
		listeCds = liste_temp;
	}
	
	public void trier(ComparateurCd compCd){
		ArrayList<CD> liste_temp=new ArrayList<CD>();
		while(listeCds.size() != 0){
			CD cd_tmp =listeCds.get(0);
			for(CD n:listeCds){
				if (compCd.etreAvant(n, cd_tmp)){
					cd_tmp = n;
				}
			}
			liste_temp.add(cd_tmp);
			listeCds.remove(cd_tmp);
		}
		listeCds = liste_temp;
	}
}

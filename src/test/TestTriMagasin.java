package test;

import static org.junit.Assert.*;

import org.junit.Test;

import donnees.CD;
import donnees.ComparateurAlbum;
import donnees.ComparateurArtiste;
import donnees.Magasin;

public class TestTriMagasin {

	@Test
	public void testTriArtiste(){
		CD PartenaireParticulier=new CD("Partenaire Particulier","Partenaire Particulier");
		CD JamesArthur=new CD("James Artur","Impossible");
		CD Hanoi=new CD("Indochine","Hanoi");
		Magasin mag=new Magasin();
		ComparateurArtiste compArt=new ComparateurArtiste();
		mag.ajouteCd(PartenaireParticulier);
		mag.ajouteCd(JamesArthur);
		mag.ajouteCd(Hanoi);
		mag.trier(compArt);
		for(int i=0; i< mag.getNombreCds(); i++){
			switch(i){
			case 1 : 
				assertEquals("Le premier devrait etre Indochine",Hanoi, mag.getCd(0));
				break;
			case 2 : 
				assertEquals("Le premier devrait etre James Arthur",JamesArthur, mag.getCd(1));
				break;
			case 3 : 
				assertEquals("Le premier devrait etre Partenaire Particulier",PartenaireParticulier, mag.getCd(2));
				break;
			}
		}
	}

	
	@Test
	public void testTriAlbum(){
		CD PartenaireParticulier=new CD("Partenaire Particulier","Partenaire Particulier");
		CD JamesArthur=new CD("James Artur","Impossible");
		CD Hanoi=new CD("Indochine","Hanoi");
		Magasin mag=new Magasin();
		ComparateurAlbum compAlb=new ComparateurAlbum();
		mag.ajouteCd(PartenaireParticulier);
		mag.ajouteCd(JamesArthur);
		mag.ajouteCd(Hanoi);
		mag.trier(compAlb);
		for(int i=0; i< mag.getNombreCds(); i++){
			switch(i){
			case 1 : 
				assertEquals("Le premier devrait etre Hanoi",Hanoi, mag.getCd(0));
				break;
			case 2 : 
				assertEquals("Le premier devrait etre Impossible",JamesArthur, mag.getCd(1));
				break;
			case 3 : 
				assertEquals("Le premier devrait etre Partenaire Particulier",PartenaireParticulier, mag.getCd(2));
				break;
			}
		}
	}
}

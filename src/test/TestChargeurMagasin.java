package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.IIOException;

import org.junit.Test;

import XML.ChargeurCD;
import XML.ChargeurMagasin;
import donnees.CD;

public class TestChargeurMagasin {

	private ChargeurMagasin c;

	@Test
	public void testChargementOkay() throws IOException {
		c=new ChargeurMagasin("musicbrainzSimple");
		c.chargerMagasin();
	}
	
	@Test(expected = FileNotFoundException.class)
	public void testChargementNotOkay() throws FileNotFoundException {
		c = new ChargeurMagasin("lol");
		c.chargerMagasin();
	}

}
